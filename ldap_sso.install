<?php

/**
 * @file
 * Update calls for ldap_sso.
 */

declare(strict_types=1);

/**
 * Moves SSO to own schema.
 */
function ldap_sso_update_8001() {
  $config_factory = \Drupal::configFactory();
  $config_old = $config_factory->getEditable('ldap_authentication.settings');
  $config_new = $config_factory->getEditable('ldap_sso.settings');
  $config_new->set('ssoExcludedPaths', $config_old->get('ssoExcludedPaths'));
  $config_new->set('ssoExcludedHosts', $config_old->get('ssoExcludedHosts'));
  $config_new->set('ssoRemoteUserStripDomainName', $config_old->get('ssoRemoteUserStripDomainName'));
  $config_new->set('seamlessLogin', $config_old->get('seamlessLogin'));
  $config_new->set('cookieExpire', $config_old->get('cookieExpire'));
  $config_new->set('ldapImplementation', $config_old->get('ldapImplementation'));
  $config_new->save(TRUE);

  $config_old->clear('ssoExcludedPaths');
  $config_old->clear('ssoExcludedHosts');
  $config_old->clear('seamlessLogin');
  $config_old->clear('ssoRemoteUserStripDomainName');
  $config_old->clear('cookieExpire');
  $config_old->clear('ldapImplementation');
  $config_old->save(TRUE);

}

/**
 * Sets server variable to REMOTE_USER as new default.
 */
function ldap_sso_update_8002() {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('ldap_sso.settings');
  $config->set('ssoVariable', 'REMOTE_USER');
  $config->save(TRUE);
}

/**
 * Sets logout redirect config variables.
 */
function ldap_sso_update_8003() {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('ldap_sso.settings');
  $config->set('redirectOnLogout', TRUE);
  $config->set('logoutRedirectPath', '/user/login');
  $config->save(TRUE);
}

/**
 * Sets login confirmation message variable.
 */
function ldap_sso_update_8004() {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('ldap_sso.settings');
  $config->set('enableLoginConfirmationMessage', TRUE);
  $config->save(TRUE);
}

/**
 * Update cookie expiration setting.
 */
function ldap_sso_update_8005() {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('ldap_sso.settings');
  if ($config->get('cookieExpire') == -1) {
    $config->set('cookieExpire', FALSE);
  }
  else {
    $config->set('cookieExpire', TRUE);
  }
  $config->save(TRUE);
}

/**
 * Remove unnecessary mod_auth declaration.
 */
function ldap_sso_update_8006() {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('ldap_sso.settings');
  if ($config->get('ldapImplementation') == 'mod_auth_kerb') {
    $config->set('ssoSplitUserRealm', TRUE);
  }
  else {
    $config->set('ssoSplitUserRealm', FALSE);
  }
  $config->clear('ldapImplementation');
  $config->save(TRUE);
}

/**
 * Fix remaining non-boolean configuration fields.
 */
function ldap_sso_update_8007() {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('ldap_sso.settings');
  $config->set('seamlessLogin', (bool) $config->get('seamlessLogin'));
  $config->set('ssoRemoteUserStripDomainName', (bool) $config->get('ssoRemoteUserStripDomainName'));
  $config->save(TRUE);
}
