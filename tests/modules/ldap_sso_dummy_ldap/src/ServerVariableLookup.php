<?php

declare(strict_types=1);

namespace Drupal\ldap_sso_dummy_ldap;

use Drupal\ldap_sso\ServerVariableLookupInterface;

/**
 * Helper function to make dummy data available in functional tests.
 */
class ServerVariableLookup implements ServerVariableLookupInterface {

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationNameFromServer(?string $variable): ?string {
    $remote_user = NULL;
    // @todo Make this path checking more robust.
    if (strpos($_SERVER['REQUEST_URI'], '/user/login/sso') !== FALSE) {
      $remote_user = 'hpotter';
    }
    return $remote_user;
  }

}
