<?php

declare(strict_types=1);

namespace Drupal\ldap_sso_dummy_ldap;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\ldap_servers_dummy\FakeBridge;
use Drupal\ldap_servers_dummy\FakeCollection;
use Drupal\ldap_servers_dummy\FakeLdap;
use Psr\Log\LoggerInterface;
use Symfony\Component\Ldap\Entry;

/**
 * Helper function to make dummy data available in functional tests.
 */
class FakeBridgeFunctional extends FakeBridge {

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($logger, $entity_type_manager);
    $this->setServerById('test');
    $this->ldap = new FakeLdap();
    $collection = [
      '(cn=hpotter)' => new FakeCollection([
        new Entry(
          'cn=hpotter,ou=people,dc=hogwarts,dc=edu',
          [
            'cn' => ['hpotter'],
            'uid' => ['123'],
            'mail' => ['hpotter@example.com'],
          ],
        ),
      ]),
    ];
    $this->ldap->setQueryResult($collection);
  }

}
